"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "user_games",
      [
        {
          name: "Sakinah",
          email: "sakinah@mail.com",
          password: "88888888",
          credit: 200000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("user_games", null, {});
  },
};

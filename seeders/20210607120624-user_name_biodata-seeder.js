"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "user_game_biodata",
      [
        {
          user_game_id: 1,
          alamat: "jaksel",
          kelamin: "wanita",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("user_game_biodata", null, {});
  },
};
